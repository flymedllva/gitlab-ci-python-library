from gcip import PagesJob, Pipeline
from tests import conftest
from gcip.addons.gitlab.jobs import pages as gitlab_pages


def test():
    pipeline = Pipeline()
    pipeline.add_children(
        gitlab_pages.asciidoctor(source="docs/index.adoc", out_file="/index.html"),
        PagesJob(),
    )

    conftest.check(pipeline.render())
