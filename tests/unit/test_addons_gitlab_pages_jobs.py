from tests import conftest
from gcip.addons.gitlab.jobs import pages


def test_asciidoctor(pipeline):
    pipeline.add_children(
        pages.asciidoctor(source="docs/index.adoc", out_file="/index.html"),
        pages.asciidoctor(source="docs/awesome.adoc", out_file="/awesome.html", job_name="pages_awesome"),
    )
    conftest.check(pipeline.render())


def test_pages_pdoc3(pipeline):
    pipeline.add_children(
        pages.pdoc3(module="gcip"),
        pages.pdoc3(module="userdoc", output_path="/user", job_name="userdoc"),
    )
    conftest.check(pipeline.render())


def test_pages_sphinx(gitlab_ci_environment_variables, pipeline):
    pipeline.add_children(pages.sphinx())
    conftest.check(pipeline.render())
