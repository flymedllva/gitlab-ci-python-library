from tests import conftest
from gcip.core.pipeline import Pipeline
from gcip.addons.container.config import DockerClientConfig
from gcip.addons.container.registries import Registry
from gcip.addons.container.sequences.copy import (
    copy_container,
)


def test_addons_container_sequences_copy_container():
    pipeline = Pipeline()
    aws_registry = Registry.AWS(account_id="1234567890123", region="eu-central-1")
    dcc = DockerClientConfig()
    dcc.add_auth(Registry.DOCKER)
    dcc.add_auth(aws_registry, "ecr-login")
    pipeline.add_children(
        copy_container(
            src_registry=Registry.DOCKER,
            dst_registry=aws_registry,
            image_name="gcip",
            image_tag="0.10.0",
            trivy_kwargs={"trivy_image": "custom/trivy:v1.2.3"},
            docker_client_config=dcc,
        ),
        stage="container",
    )
    conftest.check(pipeline.render())


def test_addons_container_sequences_copy_container_without_checks():
    pipeline = Pipeline()
    dcc = DockerClientConfig()
    dcc.add_auth(Registry.DOCKER)
    crane_config = {"crane_image": "custom/crane:1.2.1"}
    pipeline.add_children(
        copy_container(
            src_registry=Registry.DOCKER,
            dst_registry=Registry.QUAY,
            image_name="busybox",
            image_tag="latest",
            docker_client_config=dcc,
            crane_kwargs=crane_config,
            do_dive_scan=False,
            do_trivy_scan=False,
        )
    )
    conftest.check(pipeline.render())
