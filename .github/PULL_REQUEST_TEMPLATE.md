<!--Inspired by https://github.com/restic/restic-->

<!--
Thank you very much for contributing code or documentation to gcip! Please
fill out the following questions to make it easier for us to review your
changes.

You do not need to check all the boxes below all at once, feel free to take
your time and add more commits. If you're done and ready for review, please
check the last box.
-->

What does this PR change? What problem does it solve?
-----------------------------------------------------
<!--
Describe the changes and their purpose here, as detailed as needed.
-->

Was the change discussed in an issue?
------------------------------------------------------------
<!--
Link issues.

If this PR resolves an issue on GitHub, use "Closes #1234" so that the issue
is closed automatically when this PR is merged.
-->

Checklist
---------

- [ ] I have read the [Contribution Guidelines](https://github.com/dbsystel/gitlab-ci-python-library/blob/main/CONTRIBUTING.md)
- [ ] I have enabled [maintainer edits for this PR](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/allowing-changes-to-a-pull-request-branch-created-from-a-fork)
- [ ] I have added tests for all changes in this PR
- [ ] I have added api documentation for the changes
- [ ] I added a changelog entry to the unreleased section with the PR number. [Changelog](https://github.com/dbsystel/gitlab-ci-python-library/blob/main/CHANGELOG.md)
- [ ] Linting is done, and linting jobs are successfull
- [ ] I'm done, this Pull Request is ready for review
